# Weather App

This is a Weather App project for [Telerik Academy Alpha (JavaScript)](https://www.telerikacademy.com/alpha/javascript).

The App has the following logic:

1. If the browser supports Geolocation (and you allow it) AND if you do not have a "favorite" city set up yet, then the weather for the Geolocation will be shown.

2. If there is no Geolocation (the browser does not support it or you disallow it) AND if you do not have a "favorite" city set up yet, then the weather for the default city (Razgrad) will be shown.

3. If you have a "favorite" city set, then its weather will always be shown by default, skipping the Geolocation and the default city.

## How to Contribute

To be announced soon :)