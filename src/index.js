import { config } from './assets/modules/config.js';
import { data } from './assets/modules/data.js';
import { ui } from './assets/modules/ui.js';
import { helpers } from './assets/modules/helpers.js';
import { ls } from './assets/modules/localStorage.js';
import { map } from './assets/modules/map.js';

const init = () => {
  let currentCity = ls.getFavCity();

  const noGeolocation = () => {
    // Display currentCity weather info
    data.getWeather(currentCity, ui.currentTimeWeather);

    // Display the 5 days forecase
    data.getWeather(currentCity, ui.weeklyWeather, 'forecast');

    // Check if currentCity is favorite
    ui.checkFavorite(currentCity);
  };

  if ('geolocation' in navigator && !ls.checkFavCity()) {
    const successFunc = (position) => {
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;

      data.requestForecastByLocation(latitude, longitude, 'weather', 'metric')
          .then((info) => {
            currentCity = info.name;
            ui.currentTimeWeather(info);
          });

      data.requestForecastByLocation(latitude, longitude, 'forecast', 'metric')
          .then((info) => {
            ui.weeklyWeather(info);
          });
    };

    const errorFunc = () => {
      noGeolocation();
    };

    navigator.geolocation.getCurrentPosition(successFunc, errorFunc);
  } else {
    noGeolocation();
  }

  // Display default city list
  ui.clearCities();
  const cityInfoPromises = config.defaultCityList.map((city) => data.requestForecastByCity(city, 'weather', 'metric'));
  Promise
      .all(cityInfoPromises)
      .then((cityInfos) => cityInfos.forEach(ui.citiesWeather));

  // Change currentCity from default city list
  $(config.selectors.citiesUl).on('click', 'li', function() {
    currentCity = $(this).find('.city-name')[0].innerText;

    data.getWeather(currentCity, ui.currentTimeWeather);
    data.getWeather(currentCity, ui.weeklyWeather, 'forecast');

    ui.checkFavorite(currentCity);
  });

  // Change currentCity on form submit
  $(config.selectors.form).on('submit', (event) => {
    event.preventDefault();

    currentCity = helpers.newLocation();
    ui.clearInputField();

    data.getWeather(currentCity, ui.currentTimeWeather);
    data.getWeather(currentCity, ui.weeklyWeather, 'forecast');

    ui.checkFavorite(currentCity);
  });

  // Make currentCity favorite city
  $(config.selectors.favButton).on('click', () => {
    ui.makeFavorite();
    ls.setFavCity(currentCity);
  });

  map.initMap();
};

init();

// Refresh
$(config.selectors.refresh).on('click', () => {
  init();
});
