import { config } from './config.js';

const requestForecastByCity = (city, type, units) => {
  return $.get(`http://api.openweathermap.org/data/2.5/${type}?q=${city}&appid=${config.apiKey}&units=${units}`);
};

const requestForecastByLocation = (lat, lon, type, units) => {
  return $.get(`http://api.openweathermap.org/data/2.5/${type}?lat=${lat}&lon=${lon}&appid=${config.apiKey}&units=${units}`);
};

const getWeather = (city, func, type = 'weather', units = 'metric') => {
  return requestForecastByCity(city, type, units)
      .then((data) => func(data))
      .catch((err) => console.log(err));
};

const getWeatherByLocation=(lat, lon, func, type = 'weather', units = 'metric')=>{
  return requestForecastByLocation(lat, lon, type, units)
      .then((data) => func(data))
      .catch((err) => console.log(err));
};

export const data = {
  requestForecastByCity,
  requestForecastByLocation,
  getWeather,
  getWeatherByLocation,
};
