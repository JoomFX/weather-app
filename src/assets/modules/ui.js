import { config } from './config.js';

const currentTimeWeather = (data) => {
  const weatherData = {
    temp: Math.round(data.main.temp),
    cityName: data.name,
    countryName: data.sys.country,
    weatherImage: data.weather[0].icon,
    humidity: data.main.humidity,
    weatherCondition: data.weather[0].main,
    weatherConditionDesc: data.weather[0].description,
    minTemp: Math.round(data.main.temp_min),
    maxTemp: Math.round(data.main.temp_max),
    windSpeed: data.wind.speed,
    sunriseTime: (new Date(data.sys.sunrise*1000)).toLocaleTimeString(undefined, {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
    }),
    sunsetTime: (new Date(data.sys.sunset*1000)).toLocaleTimeString(undefined, {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
    }),
  };

  $(config.selectors.cityDetails).html(`<h3>${weatherData.cityName}, ${weatherData.countryName}</h3>`);
  $(config.selectors.mainWeatherImage).html(`<img src="http://openweathermap.org/img/w/${weatherData.weatherImage}.png" alt="Weather">`);
  $(config.selectors.mainTemp).html(`${weatherData.temp}`);
  $(config.selectors.weatherCondition).html(`${weatherData.weatherCondition}`);
  $(config.selectors.minTemp).html(`${weatherData.minTemp}`);
  $(config.selectors.maxTemp).html(`${weatherData.maxTemp}`);
  $(config.selectors.windSpeed).html(`${weatherData.windSpeed}`);
  $(config.selectors.humidity).html(`${weatherData.humidity}`);
  $(config.selectors.sunrise).html(`${weatherData.sunriseTime}`);
  $(config.selectors.sunset).html(`${weatherData.sunsetTime}`);
};

const weeklyWeather = (data) => {
  const today = (new Date()).getDate();
  const month = (new Date()).getMonth();
  let tomorrow;

  const dates = data.list;

  for (let i = 0; i < dates.length; i++) {
    const currentDay = new Date(dates[i].dt_txt).getDate();
    const currentMonth = new Date(dates[i].dt_txt).getMonth();

    if (currentDay > today || month < currentMonth) {
      tomorrow = i + 4;
      break;
    }
  }

  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  let html = '';

  for (let i = tomorrow; i < dates.length; i += 8) {
    const weatherData = {
      dayOfWeek: days[new Date(dates[i].dt_txt).getDay()],
      dayOfMonth: new Date(dates[i].dt_txt).getDate(),
      temp: Math.round(dates[i].main.temp),
      weatherImage: dates[i].weather[0].icon,
      weatherCondition: dates[i].weather[0].main,
    };

    html += `<li class="day-item">
      <div class="date">${weatherData.dayOfWeek} ${weatherData.dayOfMonth}</div>
      <div class="weather-image"><img src="http://openweathermap.org/img/w/${weatherData.weatherImage}.png" alt="Weather"></div>
      <div class="temperature">${weatherData.temp} <span class="temp-type">&#8451;</span></div>
      <div class="weather-condition">${weatherData.weatherCondition}</div>
    </li>`;
  }

  // If we don't have info for 12:00 o'clock on the 5th day
  if (tomorrow >= 8) {
    const weatherData = {
      dayOfWeek: days[new Date(dates[dates.length - 1].dt_txt).getDay()],
      dayOfMonth: new Date(dates[dates.length - 1].dt_txt).getDate(),
      temp: Math.round(dates[dates.length - 1].main.temp),
      weatherImage: dates[dates.length - 1].weather[0].icon,
      weatherCondition: dates[dates.length - 1].weather[0].main,
    };

    html += `<li class="day-item">
      <div class="date">${weatherData.dayOfWeek} ${weatherData.dayOfMonth}</div>
      <div class="weather-image"><img src="http://openweathermap.org/img/w/${weatherData.weatherImage}.png" alt="Weather"></div>
      <div class="temperature">${weatherData.temp} <span class="temp-type">&#8451;</span></div>
      <div class="weather-condition">${weatherData.weatherCondition}</div>
    </li>`;      
  }

  $(config.selectors.fiveDaysUl).html(html);
};

const clearCities = () => {
  $(config.selectors.citiesUl).html('');
};

const citiesWeather = (data) => {
  const weatherData = {
    temp: Math.round(data.main.temp),
    cityName: data.name,
    weatherImage: data.weather[0].icon,
    weatherCondition: data.weather[0].main,
  };

  const html = `<li class="city">
    <span class="equal-width city-name">${weatherData.cityName}</span>
    <span class="equal-width align-center day">${weatherData.weatherCondition}</span>
    <span class="equal-width align-center weather-image"><img src="http://openweathermap.org/img/w/${weatherData.weatherImage}.png" alt="Weather"></span>
    <span class="equal-width align-right temperature">${weatherData.temp} <span class="temp-type">&#8451;</span></span>
  </li>`;

  $(config.selectors.citiesUl).append(html);
};

const clearInputField = () => {
  $(config.selectors.inputField)[0].value = '';
}

const checkFavorite = (city) => {
  if (localStorage.getItem('favouriteCity') === city.toLowerCase()) {
    $(config.selectors.favButton).addClass('favorite');
  } else {
    $(config.selectors.favButton).removeClass('favorite');
  }
};

const makeFavorite = () => {
  $(config.selectors.favButton).addClass('favorite');
};

export const ui = {
  currentTimeWeather,
  weeklyWeather,
  clearCities,
  citiesWeather,
  clearInputField,
  checkFavorite,
  makeFavorite,
};
