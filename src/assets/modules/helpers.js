import { config } from './config.js';

const newLocation = () => {
  const input = $(config.selectors.inputField).val();

  return input;
};

export const helpers = { 
  newLocation,
};
