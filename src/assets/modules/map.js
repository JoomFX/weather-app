import { data } from './data.js';
import { ui } from './ui.js';
import { config } from './config.js';
// import { ls } from './localStorage.js';

const initMap = () => {
  const uluru = {
    lat: 43.5337,
    lng: 26.5411,
  };

  const map = new google.maps.Map(
      document.getElementById('googleMap'), {
        zoom: 10,
        center: uluru,
      });

  // let tempCurrentCity;

  google.maps.event.addListener(map, 'click', function(event) {
    const latitude = event.latLng.lat();
    const longitude = event.latLng.lng();

    data.getWeatherByLocation(latitude, longitude, ui.currentTimeWeather);
    data.getWeatherByLocation(latitude, longitude, ui.weeklyWeather, 'forecast');

    // CURRENTCITY = ???
    // data.requestForecastByLocation(latitude, longitude, 'weather', 'metric')
    //     .then((info) => {
    //       tempCurrentCity = info.name;
    //     });

    $(config.selectors.mapModal).modal('hide');
  });

  // Make currentCity favorite city
  // $(config.selectors.favButton).on('click', () => {
  //   ui.makeFavorite();
  //   ls.setFavCity(tempCurrentCity);
  // });
};

export const map = {
  initMap,
};
