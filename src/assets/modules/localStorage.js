import { config } from './config.js';

const setFavCity = (city) => {
  localStorage.setItem('favouriteCity', city.toLowerCase());
};

const getFavCity = () => {
  if (localStorage.getItem('favouriteCity')) {
    return localStorage.getItem('favouriteCity');
  } else {
    return config.defaultCity;
  }
};

const checkFavCity = () => {
  if (localStorage.getItem('favouriteCity')) {
    return true;
  } else {
    return false;
  }
}

export const ls = {
  setFavCity,
  getFavCity,
  checkFavCity,
};
